import urllib
import bs4
import traceback
import os

def get_page(url):
   f = urllib.urlopen(url)
   page = f.read()
   f.close()
   return page

def get_news_urls():
    urls = []
    page_num = 0

    while True:
        url = "http://www.rtvslo.si/arhiv/?date_from=2003-01-01&date_to=2012-10-16&search_type=or&section=3"
        url += "&page=" + str(page_num)
        
        print "Loading page",page_num

        try:
            page_html = get_page(url)
            page = bs4.BeautifulSoup(page_html)

            news_divs = page.find_all("div", class_="listbody")
            if news_divs is None or len(news_divs) == 0:
                return urls

            for news_div in news_divs:
                news_url = news_div.a["href"]
                urls.append(news_url)
        except Exception as e:
            print "Exception, ending on page", page_num
            traceback.print_exc()
            return urls

        page_num += 1

CATEGORY = "sport"
try:
    os.mkdir(CATEGORY)
except: pass

news_urls = get_news_urls()
f = open(CATEGORY + "/newsurls.txt", "w")
for url in news_urls:
    f.write("%s\n" % url)
f.close()

