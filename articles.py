import bs4
import urllib
import json
import itertools
from multiprocessing import Pool
from sqlalchemy import Column, Integer, UnicodeText, Unicode, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
class Article(Base):
    __tablename__ = 'articles'
    id = Column(Integer, primary_key=True)
    category = Column(Unicode)
    title = Column(UnicodeText)
    subtitles = Column(UnicodeText)
    text = Column(UnicodeText)

# Prepare DB
engine = create_engine("sqlite:///data/articles.db")
Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)

Session = sessionmaker()
Session.configure(bind=engine)

def get_article(article_id):
    url = "http://www.rtvslo.si/index.php?c_mod=news&op=print&id=" + str(article_id)
    f = urllib.urlopen(url)
    article_html = f.read()
    f.close()
    return article_html

def get_article_text(article_id):
    article_html = get_article(article_id)

    result = {}
    article = bs4.BeautifulSoup(article_html)
    result["title"] = article.title.text

    subtitles = article.find_all("div", class_="subtitle")
    subtitles = [div.text for div in subtitles]
    result["subtitles"] = subtitles

    text_content = article.find_all("p")
    text_content = " ".join([p.text for p in text_content])
    result["text"] = " ".join(text_content.split()) # This removes doubled and "strange" whitespace
    return result

def process_category(args):
    category, article_ids = args
    session = Session()
    for article_id in article_ids:
        article_data = get_article_text(article_id)
        article = Article(id=article_id, category=category, title=article_data["title"], subtitles=u"; ".join(article_data["subtitles"]), text=article_data["text"])
        session.add(article)
        print "[%s] %s" % (category, article_id)
    session.commit()

def scrape_internets(article_file):
    articles = json.loads(open(article_file, 'r').read())
    pool = Pool(processes=4)

    calls = []
    for category in articles:
        calls.extend(zip(itertools.cycle([category]), [articles[category][i::25] for i in range(25)]))
    print calls[0]
    pool.map(process_category, calls)

if __name__ == "__main__":
    scrape_internets("data/articles.json")
