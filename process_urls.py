from collections import defaultdict
import json
import os

articles = defaultdict(set)

for dir in os.listdir("."):

    if not os.path.isdir(dir): continue

    try:
        f = open(dir + "/newsurls.txt", "r")
    except: continue

    lines = f.readlines()
    f.close()

    for line in lines:
        line = line.strip('/')
        category = line[:line.find('/')]
        article_id = line[line.rfind('/'):].strip('/')
        articles[category].add(int(article_id))

for key in articles.keys():
    if len(articles[key]) < 100:
        del articles[key]
        continue

    print key,":",len(articles[key])
    articles[key] = list(articles[key])

f = open("articles.json", "w")
f.write(json.dumps(articles))
f.close()
